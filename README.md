# esc-music

Startup music for ESCs running firmwares like BLHeli32 and Bluejay

## How to use

### BLHeli_32

- Download this repository.
- Copy .txt files from blheli32 directory to the BLHeliSuite music directory:
  - macOS: `<home directory>/Documents/BLHeliSuite32xm/Music`
  - Windows: `<home folder>/Documents/BLHeliSuite32xm/Music` _(needs verifying)_
- In BLHeliSuite (v32.3.0.4 or later):
  - Connect to your ESC.
  - Select the Music Editor.
  - From the Music Script File Manager text box, select the music to use.
  - Click the load button next to the text box.
  - Click the "Apply Music" button at the bottom of the screen.
  - On the next pop-up, click yes to write the music parameters to all ESCs.
  - Disconnect your ESC.
- Plug in a battery to test the music.
- Go fly.

### Bluejay

_(Instructions to be written.)_

## How to contribute

If you would like to contribute new music, please open an
issue or open a merge request with the contents of your .txt file saved from
within the Music Editor.
